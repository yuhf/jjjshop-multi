<?php

namespace app\supplier\service;

use app\common\service\product\BaseProductService;
use app\shop\model\product\Category as CategoryModel;
use app\shop\model\settings\Delivery as DeliveryModel;
/**
 * 商品服务类
 */
class ProductService extends BaseProductService
{
    /**
     * 商品管理公共数据
     */
    public static function getEditData($model = null, $scene = 'edit', $shop_supplier_id)
    {
        // 商品分类
        $category = CategoryModel::getCacheTree();
        // 配送模板
        $delivery = DeliveryModel::getAll($shop_supplier_id);
        // 商品sku数据
        $specData = static::getSpecData($model);
        return compact('category', 'delivery', 'specData');
    }
}