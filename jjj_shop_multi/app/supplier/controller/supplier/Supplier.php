<?php

namespace app\supplier\controller\supplier;

use app\common\model\settings\Setting as SettingModel;
use app\supplier\model\supplier\Supplier as SupplierModel;
use app\supplier\controller\Controller;
/**
 * 供应商
 */
class Supplier extends Controller
{
    /**
     * 获取用户信息
     */
    public function getUserInfo()
    {
        $supplier = $this->supplier['supplier'];
        // 商城名称
        $shop_name = SettingModel::getItem('store')['name'];
        //当前系统版本
        $version = get_version();
        return $this->renderSuccess('', compact('supplier', 'shop_name', 'version'));
    }
}
