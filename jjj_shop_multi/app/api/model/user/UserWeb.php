<?php

namespace app\api\model\user;

use app\common\model\user\Sms as SmsModel;
use app\common\model\user\User as UserModel;

/**
 * 用户模型类
 */
class UserWeb extends UserModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
        'open_id',
        'is_delete',
        'app_id',
        'create_time',
        'update_time'
    ];

    /**
     * 绑定手机
     */
    public function bindMobile($user, $data)
    {
        if (!$this->check($data)) {
            return false;
        }
        return $user->save([
            'mobile' => $data['mobile']
        ]);
    }

    /**
     * 验证
     */
    private function check($data)
    {
        //判断验证码是否过期、是否正确
        $sms_model = new SmsModel();
        $sms_record_list = $sms_model
            ->where('mobile', '=', $data['mobile'])
            ->order(['create_time' => 'desc'])
            ->limit(1)->select();

        if (count($sms_record_list) == 0) {
            $this->error = '未查到短信发送记录';
            return false;
        }
        $sms_model = $sms_record_list[0];
        if ((time() - strtotime($sms_model['create_time'])) / 60 > 30) {
            $this->error = '短信验证码超时';
            return false;
        }
        if ($sms_model['code'] != $data['code']) {
            $this->error = '验证码不正确';
            return false;
        }
        return true;
    }

}
