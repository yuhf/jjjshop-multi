<?php

namespace app\api\model\page;

use app\common\model\page\CenterMenu as CenterMenuModel;

/**
 * 菜单模型
 */
class CenterMenu extends CenterMenuModel
{

    /**
     * 获取列表
     */
    public static function getMenu()
    {
        $list = self::where('status', '=', 1)->order(['sort' => 'asc', 'create_time' => 'desc'])->select();

        foreach($list as &$item){
            if(strpos($item['image_url'], 'http') !== 0){
                $item['image_url'] = base_url() .'image/menu/'. $item['image_url'];
            }
        }
        return $list;
    }
}