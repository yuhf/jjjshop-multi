<?php

namespace app\api\controller\user;

use app\api\controller\Controller;
use app\common\model\user\Sms as SmsModel;
use app\api\model\user\UserWeb as UserModel;

/**
 * h5 web用户管理
 */
class Userweb extends Controller
{
    /**
     * 短信登录
     */
    public function sendCode($mobile)
    {
        $model = new SmsModel();
        if ($model->send($mobile, 'login')) {
            return $this->renderSuccess();
        }
        return $this->renderError($model->getError() ?: '发送失败');
    }

    public function bindMobile()
    {
        $model = new UserModel;
        if ($model->bindMobile($this->getUser(), $this->request->post())) {
            return $this->renderSuccess('绑定成功');
        }
        return $this->renderError($model->getError() ?: '绑定失败');
    }
}
