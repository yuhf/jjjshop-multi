<?php

namespace app\api\controller\supplier;

use app\api\controller\Controller;
use app\api\model\product\Product as ProductModel;
use app\api\model\supplier\Supplier as SupplierModel;
/**
 * 供应商产品
 */
class Product extends Controller
{
    
    /**
     * 供应商中心主页
     */
    public function index()
    { 
        $data = $this->postData();
        $user = $this->getUser();
        // 获取商品列表数据
        $model = new ProductModel;
        $productList = $model->getList($data, $user);
        return $this->renderSuccess('', compact('productList'));
    }
   
}