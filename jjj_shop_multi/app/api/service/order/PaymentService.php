<?php

namespace app\api\service\order;
use app\common\library\easywechat\AppWx;
use app\common\library\easywechat\WxPay;
use app\common\enum\order\OrderTypeEnum;
use app\common\enum\order\OrderPayTypeEnum;
use app\common\library\helper;
use app\common\model\order\OrderTrade as OrderTradeModel;
use app\common\service\order\OrderService;

class PaymentService
{
    /**
     * 构建订单支付参数
     */
    public static function orderPayment($user, $order, $payType)
    {
        if ($payType == OrderPayTypeEnum::WECHAT) {
            return self::wechat(
                $user,
                $order['order_id'],
                $order['order_no'],
                $order['pay_price'],
                OrderTypeEnum::MASTER
            );
        }
        return [];
    }

    /**
     * 构建微信支付
     */
    public static function wechat(
        $user,
        $order_arr,
        $orderType = OrderTypeEnum::MASTER,
        $pay_source
    )
    {
        // 统一下单API
        $app = AppWx::getWxPayApp($user['app_id']);
        $open_id = $user['open_id'];

        //如果订单数大于1，则创建外部交易号
        $multiple = 0;
        if(count($order_arr) > 1){
            $orderNo = OrderService::createOrderNo();
            $payPrice =  helper::number2(helper::getArrayColumnSum($order_arr, 'pay_price'));

            //记录out_trade_no跟order_id对应关系
            foreach($order_arr as $order){
                $trade_model = new OrderTradeModel;
                $trade_list = [];
                $trade_list[] = [
                    'out_trade_no' => $orderNo,
                    'order_id' => $order['order_id'],
                    'app_id' => $order['app_id']
                ];
                $trade_model->saveAll($trade_list);
            }
            $multiple = 1;
        }else{
            $orderNo = $order_arr[0]['order_no'];
            $payPrice = $order_arr[0]['pay_price'];
        }

        $WxPay = new WxPay($app);
        $payment = $WxPay->unifiedorder($orderNo, $open_id, $payPrice, $orderType, $pay_source, $multiple);
        if($pay_source == 'wx'){
            return $payment;
        }else if($pay_source == 'mp'){
            $jssdk = $app->jssdk;
            return $jssdk->bridgeConfig($payment['prepay_id']);
        }else if($pay_source == 'payH5'){
            return $payment;
        }else if($pay_source == 'app'){
            return $payment;
        }
    }
}