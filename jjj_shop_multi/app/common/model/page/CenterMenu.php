<?php

namespace app\common\model\page;

use think\facade\Request;
use app\common\model\BaseModel;

/**
 * 个人中心菜单模型
 */
class CenterMenu extends BaseModel
{
    protected $name = 'center_menu';
    protected $pk = 'menu_id';

    /**
     * 获取列表
     */
    public function getList($limit = 20)
    {   
        $list = $this->order(['sort' => 'asc'])
            ->paginate($limit);
        foreach($list as &$item){
            if(strpos($item['image_url'], 'http') !== 0){
                $item['image_url'] = base_url() .'image/menu/'. $item['image_url'];
            }
        }
        return $list;
    }

    /**
     * 详情
     */
    public static function detail($menu_id)
    {
        $model = self::find($menu_id);
        if(strpos($model['image_url'], 'http') !== 0){
            $model['image_url'] = base_url() .'image/menu/'. $model['image_url'];
        }
        return $model;
    }
  
}