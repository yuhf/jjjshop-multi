<?php

namespace app\shop\controller\supplier;

use app\shop\controller\Controller;
use app\shop\model\supplier\Supplier as SupplierModel;
use app\shop\model\supplier\Category as CategoryModel;
/**
 * 供应商控制器
 */
class Supplier extends Controller
{

    /**
     * 店员列表
     */
    public function index()
    {
        // 供应商列表
        $model = new SupplierModel;
        $list = $model->getList($this->postData());
        return $this->renderSuccess('', compact('list'));
    }

    /**
     * 添加供应商
     */
    public function add()
    {
        $model = new SupplierModel;
        $category = CategoryModel::getALL();
        if($this->request->isGet()){
            return $this->renderSuccess('', compact('category'));
        }
        // 新增记录
        if ($model->add($this->postData())) {
            return $this->renderSuccess('', '添加成功');
        }
        return $this->renderError($model->getError() ?: '添加失败');
    }


    /**
     * 编辑供应商
     */
    public function edit($shop_supplier_id)
    {
        $model = SupplierModel::detail($shop_supplier_id, ['logo', 'business', 'superUser.user']);
        $category = CategoryModel::getALL();
        if($this->request->isGet()){
            return $this->renderSuccess('', compact('model','category'));
        }
        if ($model->edit($this->postData())) {
            return $this->renderSuccess('', '更新成功');
        }
        return $this->renderError($model->getError() ?: '更新失败');
    }

    /**
     * 删除店员
     */
    public function delete($shop_supplier_id)
    {
        // 店员详情
        $model = SupplierModel::detail($shop_supplier_id);
        if (!$model->setDelete()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('', $model->getError() ?: '删除成功');
    }
     /**
     * 开启禁止
     */
    public function recycle($shop_supplier_id, $is_recycle)
    {
        // 商品详情
        $model = SupplierModel::detail($shop_supplier_id);
        if (!$model->setRecycle($is_recycle)) {
            return $this->renderError('操作失败');
        }
        return $this->renderSuccess('操作成功');
    }
}
