import config from '../config.js'
/*导航菜单白名单*/
const tabBarLinks = [
	'/pages/index/index',
	'/pages/product/category',
	'/pages/shop/middle',
	'/pages/cart/cart',
	'/pages/user/index/index'
];

/*
 * 跳转页面
 */
export const gotopage = (url) => {
	if (!url || url.length == 0) {
		return false;
	}
	
	if(url.substr(0,1)!=='/'){
		url='/' + url;
	}
	let p = url;
	if(url.indexOf('?') != -1){
		p = url.substr(0, url.indexOf('?'));
	}
	// tabBar页面
	if (tabBarLinks.indexOf(p) > -1) {
		uni.reLaunch({
			url: url
		})
	} else {
		// 普通页面
		uni.navigateTo({
			url:  url
		});
	}
}
